<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryImageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CategoryImageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryImageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CategoryImage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/category-image');
        CRUD::setEntityNameStrings('category image', 'category images');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setColumns([
            [
                'type'          =>  'select',
                'name'          =>  'category_id',
                'entity'         =>  'category',
                'attribute'     =>  'name',
                'model'         =>  'App\Models\Category',
            ],
            [
                'type'          =>  'text',
                'name'          =>  'year',
            ],
            [
                'type'          =>  'image',
                'name'          =>  'image',
            ],

            [
                'type'          =>  'text',
                'name'          =>  'image_name',
            ],
        ]);

        // Filter
        $vehicleArray = array('' => 'Select');
        $vehicleData = \App\Models\Category::where('category_status', '1')->get();
        
        if($vehicleData)
        {
            foreach($vehicleData as $vehicleRow)
            {
                $vehicleArray[$vehicleRow->id] = $vehicleRow->name;
            }
        }
        $this->crud->addFilter([ // date filter
          'name' => 'category_id',
          'label'=> 'Category',
          'type' => 'select2'
        ],
        $vehicleArray,
        function($value) { // if the filter is active, apply these constraints
          $this->crud->addClause('where', 'category_id', '=', $value);
        });

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CategoryImageRequest::class);

        $year = date('Y');
        $yearData = array_combine(range($year, 2015), range($year, 2015));

        $this->crud->addFields([
            [
                'type'          =>  'select2',
                'name'          =>  'category_id',
                'entity'         =>  'category',
                'attribute'     =>  'name',
                'model'         =>  'App\Models\Category',
            ],
            [
                'type'          =>  'select2_from_array',
                'name'          =>  'year',
                'options'       => $yearData
            ],
            [
                'type'          =>  'select2_from_array',
                'name'          =>  'image_type',
                'options'         =>  ['portrait' => 'Portrait', 'landscape' => 'Landscape']
            ],
            [
                'type'          =>  'text',
                'name'          =>  'image_name',
                'label'         =>  'Image Name'
            ],
            [
                'type'          =>  'browse',
                'name'          =>  'image',
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
