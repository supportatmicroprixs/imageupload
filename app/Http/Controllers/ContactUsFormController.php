<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Category;
use Mail;
use Session;

class ContactUsFormController extends Controller {

    // Create Contact Form
    public function createForm(Request $request) {
        return view('contact');
    }

    // Store Contact Form data
    public function ContactUsForm(Request $request) {

        // Form validation
        $this->validate($request, [
            'name' => 'required',
            'email' => '',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|numeric|min:10',
            
         ]);

        $data = array('name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'catname' => $request->catname, 'image' => $request->image);

        $contact = new Contact;
        $contact->name = $data['name'];
        $contact->email = $data['email'] ?? '';
        $contact->phone = $data['phone'];
        $contact->catname = $data['catname'];
        $contact->image = $data['image'];

        $contact->save();
        //  Store data in database

        session ( [
            'is_download' => 1
        ] );

        return back()->with('success', 'Your image will be downloaded shortly.');
        //return back()->withInput('success', 'Your image will be downloaded shortly.');
    }

}