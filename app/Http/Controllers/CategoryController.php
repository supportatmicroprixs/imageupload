<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
	public function getDetails($id)
	{
    	$categories = Category::where('id', '=', $id)->first();
    	
    	return view('welcome', ['categories' => $categories]);
	}
}