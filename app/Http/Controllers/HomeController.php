<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Category;
use Session;

class HomeController extends Controller
{
	public function index()
	{
		$homecategory = \DB::table('categories')->orderBy('id','ASC')->get();
    	return view('welcome', ['categories' => $homecategory]);
    }

    public function getDetails(Request $request, $category_id)
    {
        $id = $request->id;
        $year = $request->year;

        if($year)
        {
            $categorieExists = \DB::table('categorie_images')->where('category_id', '=', $category_id)->where('year', $year)->count();
            $categoriePage = \DB::table('categorie_images')->where('category_id', '=', $category_id)->where('year', $year)->get();
        }
        else
        {
            $categorieExists = \DB::table('categorie_images')->where('category_id', $category_id)->count();
            $categoriePage = \DB::table('categorie_images')->where('category_id', $category_id)->get();
        }

        $categoryName = \DB::table('categories')->get();
        
        $yearDropDown = \DB::table('categorie_images')->select('year')->groupBy('year')->get();

		return view('category', ['category_id' => $category_id, 'categorieExists' => $categorieExists, 'categoriePage' => $categoriePage, 'categoryName' => $categoryName, 'yearDropDown' => $yearDropDown, 'year' => $year]);
    }

    public function getCategoryYear(Request $request, $category_id, $year)
    {
        if($year)
        {
            $categorieExists = \DB::table('categorie_images')->where('category_id', '=', $category_id)->where('year', $year)->count();
            $categoriePage = \DB::table('categorie_images')->where('category_id', '=', $category_id)->where('year', $year)->get();
        }
        else
        {
            $categorieExists = \DB::table('categorie_images')->where('category_id', $category_id)->count();
            $categoriePage = \DB::table('categorie_images')->where('category_id', $category_id)->get();
        }

        $categoryName = \DB::table('categories')->get();
        
        $yearDropDown = \DB::table('categorie_images')->select('year')->groupBy('year')->get();

        return view('category', ['category_id' => $category_id, 'categorieExists' => $categorieExists, 'categoriePage' => $categoriePage, 'categoryName' => $categoryName, 'yearDropDown' => $yearDropDown, 'year' => $year]);
    }

    public function getImageDetails($category_id, $id)
    {
        $categoriePage1 = \DB::table('categorie_images')->where('id', '=', $id)->first();
        
        $image = $categoriePage1->image;

        $categoriePage = \DB::table('categories')->where('id', '=', $category_id)->orderBy('id','ASC')->first();
        $name = $categoriePage->name;

        $categoryName1 = \DB::table('categories')->orderBy('id','ASC')->get();

        $latestcontact = \DB::table('contacts')->orderBy('id','DESC')->first();

        $is_download = session()->get('is_download');

        return view('category-detail', ['categoriePage' => $categoriePage, 'categoryName1' => $categoryName1, 'categoriePage1' => $categoriePage1,'latestcontact' => $latestcontact, 'image' => $image, 'name' => $name, 'is_download' => $is_download]);   
    }

    public function clearSession()
    {
        session()->forget('is_download');
        # code...
    }
}