<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use CrudTrait;
    protected $table = 'categories';
    protected $guarded = ['id'];
    public $fillable = ['name'];
}
