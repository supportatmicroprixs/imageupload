<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

use Illuminate\Database\Eloquent\Model;

class CategoryImage extends Model
{
    use CrudTrait;
    protected $table = 'categorie_images';
    protected $guarded = ['id'];
    public $fillable = ['category_id', 'year' ,'image', 'image_type', 'image_name'];

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
