@include('common.header')
<style type="text/css">

  .container {
    max-width: 500px;
    margin: 50px auto;
    text-align: left;
    font-family: sans-serif;
}

form {
    border: 1px solid #1A33FF;
    background: #ecf5fc;
    padding: 40px 50px 45px;
}

.form-control:focus {
    border-color: #000;
    box-shadow: none;
}

label {
    font-weight: 600;
}

.error {
    color: red;
    font-weight: 400;
    display: block;
    padding: 6px 0;
    font-size: 14px;
}

.form-control.error {
    border-color: red;
    padding: .375rem .75rem;
}

div#social-links {
    margin: 0 auto;
    max-width: 500px;
}
div#social-links ul li {
    display: inline-block;
}          
div#social-links ul li a {
    padding: 20px;
    border: 1px solid #ccc;
    margin: 1px;
    font-size: 30px;
    color: #222;
    background-color: #ccc;
}

@media(max-width: 400px)
{
  div#kt_aside_logo{ display: none; }
}
</style>

  <!--begin::Body-->
  <body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
    <!--begin::Main-->
    <!--begin::Root-->
    <div class="d-flex flex-column flex-root">
      <!--begin::Page-->
      <div class="page d-flex flex-row flex-column-fluid">
        <!--begin::Aside-->
        <div id="kt_aside" class="aside aside-dark aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
          <!--begin::Brand-->
          <div class="aside-logo flex-column-auto" id="kt_aside_logo">
            <!--begin::Logo-->
            <a href="{{ asset('/') }}">
              <img alt="Logo" src="{{ asset('/') }}{{ SITE_LOGO }}" class="h-85px logo" />
            </a>
            <!--end::Logo-->
            <!--begin::Aside toggler-->
            <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
              <!--begin::Svg Icon | path: icons/duotune/arrows/arr079.svg-->
              <span class="svg-icon svg-icon-1 rotate-180">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                  <path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="black" />
                  <path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="black" />
                </svg>
              </span>
              <!--end::Svg Icon-->
            </div>
            <!--end::Aside toggler-->
          </div>
          <!--end::Brand-->
          <!--begin::Aside menu-->
          <div class="aside-menu flex-column-fluid">
            <!--begin::Aside Menu-->
            <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
              <!--begin::Menu-->
              <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true">
                <div data-kt-menu-trigger="click" class="menu-item here show menu-accordion">
                  <span class="menu-link">
                    <span class="menu-icon">
                      <!--begin::Svg Icon | path: icons/duotune/finance/fin006.svg-->
                      <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                          <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="black" />
                          <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="black" />
                        </svg>
                      </span>
                      <!--end::Svg Icon-->
                    </span>
                    <span class="menu-title">Categories</span>
                    <span class="menu-arrow"></span>
                  </span>
                  <div class="menu-sub menu-sub-accordion">

                  @foreach($categoryName1 as $categorys)
                  <div class="menu-sub menu-sub-accordion">
                    <div class="menu-item">
                      <a class="menu-link" href="{{ asset('/category') }}/{{ $categorys->id }}">
                        <span class="menu-bullet">
                          <span class="bullet bullet-dot"></span>
                        </span>
                        <span class="menu-title">{{ $categorys->name }}</span>
                      </a>
                    </div>
                  </div>
                  @endforeach
                  </div>
                </div>
              </div>
              <!--end::Menu-->
            </div>
            <!--end::Aside Menu-->
          </div>
          <!--end::Aside menu-->
        </div>
        <!--end::Aside-->
        <!--begin::Wrapper-->
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
          <!--begin::Header-->
          <div id="kt_header" style="background:#3E4095;" class="header align-items-stretch">
            <!--begin::Container-->
            <div class="container-fluid d-flex align-items-stretch justify-content-between">
              <!--begin::Aside mobile toggle-->
              <div class="d-flex align-items-center d-lg-none ms-n3 me-1" title="Show aside menu">
                <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
                  <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                  <span class="svg-icon svg-icon-2x mt-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                      <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                    </svg>
                  </span>
                  <!--end::Svg Icon-->
                </div>
              </div>
              <!--end::Aside mobile toggle-->
              <!--begin::Mobile logo-->
              <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
                <a href="{{ asset('/') }}" class="d-lg-none">
                  <img alt="Logo" src="../{{ SITE_LOGO }}" class="h-30px" />
                </a>
              </div>
              <!--end::Mobile logo-->
            </div>
            <!--end::Container-->
          </div>
          <!--end::Header-->
          <!--begin::Content-->
          <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <!--begin::Toolbar-->
            <div class="toolbar" id="kt_toolbar">
              <!--begin::Container-->
              <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack"></div>
              <!--end::Container-->
            </div>
            <!--end::Toolbar-->
            <!--begin::Post-->
            <div class="post d-flex flex-column-fluid" id="kt_post">
              <!--begin::Container-->
              <div id="kt_content_container" class="container-xxl">
                <!--begin::Layout-->
                <div class="d-flex flex-column flex-xl-row">
                  <!--begin::Sidebar-->
                  <div class="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10">
                    <!--begin::Card-->
                    <div class="card mb-5 mb-xl-8">
                      <!--begin::Card body-->
                      <div class="pt-15">
                        <!--begin::Summary-->
                        <div class="d-flex flex-center flex-column mb-5">
                          <!--begin::Avatar-->
                          <?php 
                          if($categoriePage1 != "") { ?>
                          <script src="{{ asset('/') }}public/js/jquery.min.js"></script>
                          <script src="{{ asset('/') }}public/js/html2canvas.js"></script>
                          <center>
                              
                          <div id="html-content-holder" style="background-color: #F0F0F1; width: 335px;">
                            <div class="row" style="background-color:#fff;border-bottom-left-radius: 0%; border-bottom-right-radius: 0%;border: 1px solid gray;width:100%;">
                              
                            </div>
                               <center>
                                <img src="{{ asset('/').$image }}" style="max-width: 100%">
                              </center>
                              <div class="row" style="background-color:#fff;padding-bottom:5px;padding-top:5px;border-top-left-radius: 0%;width:100%; border-top-right-radius: 0%;border: 1px solid gray;">
                              
                              <div class="col-md-12">
                                @if($is_download && $latestcontact)
                                @if($latestcontact->name != '')
                                <span style="float: left;color:#000;font-weight:500;font-size:11px;" id="imageuserName"> <i class="fa fa-user"></i> {{ $latestcontact->name; }}</span>@endif

                                @if($latestcontact->email != '')
                                <span style="text-align: center;color:#000;font-weight:500;font-size:11px;" id="imageuserEmail"> <i class="fa fa-envelope"></i>{{ $latestcontact->email; }}</span>@endif

                                @if($latestcontact->phone != '')
                                <span style="float: right;color:#000;font-weight:500;font-size:11px;" id="imageuserPhone"> <i class="fa fa-mobile"></i>{{ $latestcontact->phone; }}</span>@endif
                                @else
                                <span style="float: left;color:#000;font-weight:500;font-size:11px;" id="imageuserName"> <i class="fa fa-user"></i> </span>
                                <span style="text-align: center;color:#000;font-weight:500;font-size:11px;" id="imageuserEmail"> <i class="fa fa-envelope"></i></span>
                                <span style="float: right;color:#000;font-weight:500;font-size:11px;" id="imageuserPhone"> <i class="fa fa-mobile"></i></span>
                                @endif
                            </div>
                          </div>
                        </div><br>
                        <a id="btn-Convert-Html2Image" class="btn btn-success d-none" onclick="downloadImage();">Download</a>
                          </center>
                          <?php } ?>
                        </div>
                        <!--end::Summary-->
                      </div>
                      <!--end::Card body-->
                    </div>
                  </div>
                  <!--end::Sidebar-->
                  <!--begin::Content-->
                  <div class="flex-lg-row-fluid ms-lg-15" style="position: relative;top: 50px;">
                    <div class="tab-content" id="myTabContent">
                     <div class="tab-pane fade show active" id="kt_customer_view_overview_tab" role="tabpanel">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif

                        <form method="post" name="myForm" class="form" action="{{ route('contact.store')}}" id="contactForm">

                        @csrf

                        <div class="form-group">
                            <label>Name<error style="color: red;">*</error></label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="name" id="name" onkeyup="setName(this.value);">

                            <!-- Error -->
                            @if ($errors->has('name'))
                            <div class="error">
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" id="email" onkeyup="setEmail(this.value);">

                        </div>

                        <div class="form-group">
                            <label>Phone<error style="color: red;">*</error></label>
                            <input type="text" class="form-control {{ $errors->has('name') ? 'error' : '' }}" name="phone" id="phone" onkeyup="setPhone(this.value);" maxlength="10">

                             <!-- Error -->
                            @if ($errors->has('phone'))
                            <div class="error">
                                {{ $errors->first('phone') }}
                            </div>
                            @endif
                        </div>
                        
                          <input type="hidden" name="catname" value="{{ $name }}">
                          <input type="hidden" name="image" value="{{ $image }}">
                        <br>
                        <input type="submit" name="send" id="skbtn" value="Download" class="btn btn-dark btn-block" onclick="myFunction();" />
                    </form>
                      </div>
                    </div>
                    <!--end:::Tab content-->
                  </div>
                  <!--end::Content-->
                </div>
              </div>
              <!--end::Container-->
            </div>
            <!--end::Post-->
          </div>
          
          <!--begin::Footer-->
          <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
            <!--begin::Container-->
            <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
              <!--begin::Copyright-->
              <div class="text-dark order-2 order-md-1">
                <span class="text-muted fw-bold me-1">{{ now()->year }}©</span>
                <a href="{{ SITE_REDIRECT }}" target="_blank" class="text-gray-800 text-hover-primary">{{ SITE_NAME }}</a>
              </div>
            </div>
            <!--end::Container-->
          </div>
          <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
      </div>
      <!--end::Page-->
    </div>
    </div>
    <div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
      <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
      <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
          <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
          <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
      </span>
      <!--end::Svg Icon-->
    </div>
    <!--end::Scrolltop-->

<!-- <script>
document.onkeydown = function(e) {
  if (e.ctrlKey && 
      (e.keyCode === 67 || 
       e.keyCode === 86 || 
       e.keyCode === 85 || 
       e.keyCode === 117)) {
      return false;
  } else {
      return true;
  }
};
$(document).keypress("u",function(e) {
  if(e.ctrlKey)
  {
return false;
}
else
{
return true;
}
});
</script> -->
<!-- <script>
document.addEventListener("contextmenu", function(event){
event.preventDefault();

}, false);
</script> -->

<script type="text/javascript">
  @if($is_download != "")
    setTimeout(function(){
      document.getElementById('btn-Convert-Html2Image').click();

      // ajax call for unset session
      var base_url = $('base').attr('href');
  
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: base_url+'clearSession',
        type: 'post',
        data: {_token: CSRF_TOKEN},
        beforeSend: function() {
        },
        success: function(output) {
        }
      });
    }, 2000);
  @endif    
  
  function setName(namevalue)
  {
    $('#imageuserName').html(namevalue);
  }

  function setEmail(emailvalue)
  {
    $('#imageuserEmail').html(emailvalue);
  }

  function setPhone(phonevalue)
  {
    $('#imageuserPhone').html(phonevalue);
  }
</script>

<script type="text/javascript">
  var element = $("#html-content-holder"); // global variable
  var imgtitle = 'finance';
  var imgext = '';

  var getCanvas; // global variable
   
  
    html2canvas(element, {
      onrendered: function (canvas) 
      {
        getCanvas = canvas;
      }

  });

function downloadImage(){
    var imgageData = getCanvas.toDataURL("image/png");  
    
    // Now browser starts downloading it instead of just showing it
    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
    //$("#btn-Convert-Html2Image").attr("download", "vibscampaign.png").attr("href", newData);d

      $("#btn-Convert-Html2Image").attr("download", `SK-${imgtitle}.png`).attr("href", newData);
       //$('#btn-Convert-Html2Image').trigger('click');
}

</script>


    <!--end::Main-->
    <script>var hostUrl = "assets/";</script>
    <script src="{{ asset('/') }}public/js/plugins.bundle.js"></script>
    <script src="{{ asset('/') }}public/js/scripts.bundle.js"></script>
    <script src="{{ asset('/') }}public/js/datatables.bundle.js"></script>
    <script src="{{ asset('/') }}public/js/payment-table.js"></script>
    <script src="{{ asset('/') }}public/js/new-card.js"></script>
    <script src="{{ asset('/') }}public/js/widgets.js"></script>
    <script src="{{ asset('/') }}public/js/chat.js"></script>
    <script src="{{ asset('/') }}public/js/create-app.js"></script>
    <script src="{{ asset('/') }}public/js/upgrade-plan.js"></script>
    <script src="{{ asset('/') }}public/js/intro.js"></script>
    <noscript>
      <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!--End::Google Tag Manager (noscript) -->
  </body>
  <!--end::Body-->
</html>