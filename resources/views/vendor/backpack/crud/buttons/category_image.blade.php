@if ($crud->hasAccess('list'))
	<div class="btn-group">
		<a class="btn btn-sm btn-link dropdown-toggle text-primary pl-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			Extra <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu dropdown-menu-right">
		  	<li>
				<a target="_blank" class="dropdown-item" href="{{ backpack_url('category-image?category_id='.$entry->getKey()) }}"><i class="fa fa-image"></i> Category Image</a>
			</li>
		</ul>
	</div>
@endif
