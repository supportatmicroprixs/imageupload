<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactUsFormController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Models\Setting;
use App\Models\Category;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Setting::assignSetting();

Route::get('/', [HomeController::class, 'index']);

Route::get('/category/{id}', [HomeController::class, 'getDetails']);
Route::get('/category-year/{id}/{year}', [HomeController::class, 'getCategoryYear']);

Route::get('/category-detail/{category_id}/{id}', [HomeController::class, 'getImageDetails']);
Route::post('/clearSession', [HomeController::class, 'clearSession']);

Route::get('/contact', [ContactUsFormController::class, 'createForm']);

Route::post('/contact', [ContactUsFormController::class, 'ContactUsForm'])->name('contact.store');
Route::get('/social-media-share', [SocialShareButtonsController::class,'ShareWidget']);